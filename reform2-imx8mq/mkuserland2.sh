#!/bin/bash

chroot target-userland /bin/bash <<EOF
export LC_ALL=C
export LANGUAGE=C
export LANG=C

set -e
set -x

cd /root
mkdir -p src
cd src

git clone --depth 1 --branch libdrm-2.4.106 https://gitlab.freedesktop.org/mesa/drm.git

# there's no tag yet for 21.1.3 which will probably include all patches we need
# so fetch the most recent commit at the time of putting this script together.
mkdir -p mesa
cd mesa
git init
git remote add origin https://gitlab.freedesktop.org/mesa/mesa.git
git fetch origin 2ebf4e984b51825c37562e0221bde327188e3eaf
git reset --hard FETCH_HEAD
cd ..

mkdir -p xserver
cd xserver
git init
git remote add origin https://gitlab.freedesktop.org/xorg/xserver.git
git fetch origin f3eb1684fa5008ad7c881f798a5efb7441b23035
git reset --hard FETCH_HEAD
cd ..

git clone https://github.com/swaywm/wlroots.git
git clone https://github.com/swaywm/sway.git
git clone --depth 1 --branch 0.9.7 https://github.com/Alexays/Waybar.git
git clone --depth 1 --branch v0.4.0 https://github.com/any1/wayvnc.git
git clone --depth 1 --branch v0.1.3 https://github.com/Hjdskes/cage.git

cd drm
meson build -Detnaviv=true -Dradeon=false -Damdgpu=false -Dvmwgfx=false -Dfreedreno=false -Dvc4=false -Dnouveau=false
ninja -C build install
cd ..
rm -rf drm

ldconfig

cd mesa
meson build -Dplatforms=x11,wayland -Ddri3=true -Dgallium-drivers=swrast,etnaviv,kmsro,virgl -Dgbm=enabled -Degl=enabled -Dbuildtype=release -Db_ndebug=true
ninja -C build install
cd ..
rm -rf mesa

ldconfig

cd wlroots
git checkout 0.13.0
meson build
ninja -C build install
cd ..
rm -rf wlroots

ldconfig

cd sway
git checkout 1.6
meson build
ninja -C build install
chmod +s /usr/local/bin/sway
cd ..
rm -rf sway

cd xserver

# patch to work around flickery GTK2 and other legacy X11 GUIs on etnaviv

patch -p1 <<ENDPATCH
diff --git a/glamor/glamor_render.c b/glamor/glamor_render.c
index be0741a..1dd2876 100644
--- a/glamor/glamor_render.c
+++ b/glamor/glamor_render.c
@@ -1584,6 +1584,8 @@ glamor_composite_clipped_region(CARD8 op,
     if (prect != rect)
         free(prect);
  out:
+    glFinish();
+
     if (temp_src != source)
         FreePicture(temp_src, 0);
     if (temp_mask != mask)
ENDPATCH

meson build -Dxorg=true -Dxwayland=true -Dglamor=true -Dxwayland_eglstream=false -Dxnest=false -Ddmx=false -Dxvfb=true -Dxwin=false -Dxephyr=false -Ddri3=true
ninja -C build install
cd ..
rm -rf xserver

# overwrite /usr/bin/Xwayland with symlink to our Xwayland (FIXME: brittle)

rm -f /usr/bin/Xwayland
ln -s /usr/local/bin/Xwayland /usr/bin/Xwayland

cd Waybar
meson build
ninja -C build install
cd ..
rm -rf Waybar

cd wayvnc
mkdir subprojects
cd subprojects
git clone https://github.com/any1/neatvnc.git
git clone https://github.com/any1/aml.git
cd ..
meson build
ninja -C build install
cd ..
rm -rf wayvnc

cd cage
meson build
ninja -C build install
cd ..
rm -rf cage

EOF
