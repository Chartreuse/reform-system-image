#!/bin/bash

set -e
set -x

ETC=./template-etc
SKEL=./template-skel

mmdebstrap --architectures=arm64 --components=main --variant="minbase" --include="apt apt-utils gnupg ca-certificates cpio bsdmainutils init-system-helpers procps gpgv debian-archive-keyring readline-common cron netbase iproute2 ifupdown isc-dhcp-client iptables iputils-ping locales less net-tools curl wget nano micro vim coreutils parted file git sudo console-setup console-data unicode-data kbd gpm systemd libpam-systemd systemd-sysv bash-completion ncurses-term alsa-utils brightnessctl brightness-udev usbutils pciutils fbset netcat-traditional nfacct traceroute wpasupplicant htop ncdu ntpdate ntp screen tmux telnet lm-sensors rfkill dosfstools e2fsprogs dialog rsync busybox pulseaudio  sway grim slurp xwayland xterm xfce4-terminal rofi arc-theme thunar policykit-1 libblockdev-dm2 libblockdev-crypto2 gnome-disk-utility cryptsetup openjdk-11-jre-headless  gnome-system-monitor eog evince mpv gedit engrampa connman-gtk gnome-icon-theme breeze-icon-theme fonts-noto-color-emoji lxpolkit mesa-utils w3m man-db pavucontrol python3-psutil ircii elinks  zlib1g-dev patch expat bison flex libunwind-dev libwayland-dev wayland-protocols libwayland-egl-backend-dev libx11-dev libx11-xcb-dev libxdamage-dev libxfixes-dev libxcb-dri3-dev libxcb-xfixes0-dev libxcb-sync-dev libxrandr-dev libxext-dev libxcb-glx0-dev libxcb-present-dev libxcb-dri2-0-dev gettext autopoint libpixman-1-dev libbsd-dev libxkbfile-dev libxcb-composite0-dev libxcb-xinput-dev libxcb-icccm4-dev libxfont-dev nettle-dev libdbus-1-dev libsystemd-dev libpciaccess-dev llvm-dev libudev-dev libmtdev-dev libevdev-dev libxshmfence-dev xutils-dev libdrm-dev libxxf86vm-dev meson gcc g++ python3-setuptools python3-mako xfonts-utils libepoxy-dev libjson-c-dev libpcre3-dev libpango1.0-dev libxkbcommon-dev libinput-dev autoconf make libtool intltool libxml2-dev libxfce4ui-2-dev libgarcon-1-dev xfce4-dev-tools libdbusmenu-gtk3-dev libfmt-dev libgirepository1.0-dev libgtkmm-3.0-dev libjsoncpp-dev libmpdclient-dev libnl-3-dev libnl-genl-3-dev libpulse-dev  libsigc++-2.0-dev libspdlog-dev  libsdl2-gfx-dev libsdl2-mixer-dev libsdl2-net-dev libsdl2-ttf-dev libsdl2-dev libsdl2-image-dev libflac-dev libmpg123-dev libpng-dev libmpeg2-4-dev" sid target-userland http://ftp.de.debian.org/debian

# install kernel
cp linux/arch/arm64/boot/Image target-userland/
# install DTBs
cp linux/arch/arm64/boot/dts/freescale/imx8mq-mnt-reform2.dtb target-userland/imx8mq-mnt-reform2-single-display.dtb
cp linux/arch/arm64/boot/dts/freescale/imx8mq-mnt-reform2-hdmi.dtb target-userland/imx8mq-mnt-reform2-dual-display.dtb
# default to single display (less flickery)
cp linux/arch/arm64/boot/dts/freescale/imx8mq-mnt-reform2.dtb target-userland/imx8mq-mnt-reform2.dtb
# provide a copy of u-boot for (re)flashing
mkdir -p target-userland/boot
cp ./u-boot/flash.bin target-userland/boot/
cp ./u-boot/flash-rescue.bin target-userland/boot/
cp ./u-boot/flash-rescue.sh target-userland/boot/

# default audio settings, or PCM will be muted
mkdir -p target-userland/var/lib/alsa
cp $ETC/asound.state target-userland/var/lib/alsa/

# populate root user and skel
cp -RavT $SKEL target-userland/root/
cp -RavT $SKEL target-userland/etc/skel/
mkdir -p target-userland/etc/skel/Desktop
mkdir -p target-userland/etc/skel/Documents
mkdir -p target-userland/etc/skel/Downloads
mkdir -p target-userland/etc/skel/Music
mkdir -p target-userland/etc/skel/Pictures
mkdir -p target-userland/etc/skel/Videos

# populate /etc
cp $ETC/ld.so.conf target-userland/etc # or our libraries won't be picked up
mkdir -p target-userland/etc/dhcp
cp $ETC/dhclient.conf target-userland/etc/dhcp
cp $ETC/motd-rescue target-userland/etc/motd # we start with the rescue system
cp $ETC/hostname target-userland/etc
cp $ETC/hosts target-userland/etc
cp $ETC/adduser.conf target-userland/etc # or else EXTRA_GROUPS doesn't work?!
cp $ETC/reform-* target-userland/etc

# copy pulse config files that will be moved inside the target fs later
cp $ETC/pulse/* target-userland/

# custom built libraries
mkdir -p target-userland/root/src

chroot target-userland /bin/bash <<EOF
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
export LC_ALL=C
export LANGUAGE=C
export LANG=C

set -x
set -e

echo "root:root" | chpasswd
passwd -d root # remove root password

ln --force --symbolic /usr/share/zoneinfo/Europe/Berlin /etc/localtime
dpkg --configure -a

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/default/locale

# add MNT Research/Reform apt repository
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 376511EB67AD7BAF

echo "deb https://mntre.com/reform-debian sid/" >>/etc/apt/sources.list
apt update

# install essential MNT Reform system management scripts and docs
apt install -y reform-tools reform-handbook

# move pulse config in place
# FIXME: this is brittle and should go into a .deb
mv /analog-input-reform.conf /usr/share/pulseaudio/alsa-mixer/paths/
mv /default.conf /usr/share/pulseaudio/alsa-mixer/profile-sets/

# move hardware setup one-shot service in place
mv /etc/reform-hw-setup.service /etc/systemd/system/
systemctl enable reform-hw-setup.service

# remove some unnecessary stuff (apparmor slows down boot)
apt remove -y ofono foot apparmor

# disable built-in sleep targets in favor of reform-standby script
systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

EOF
