#!/bin/bash

losetup -o $((4 * 1024 * 1024)) /dev/loop0 ./reform-system.img
mount /dev/loop0 /mnt

echo "When you're done, use: losetup -d /mnt"

